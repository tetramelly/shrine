CC      := g++
CCFLAGS := -Iinclude
LDFLAGS := -lglfw -lGL -lGLEW -lSOIL

TARGETS := shrine
OBJ     := shrine.o input.o vertices.o shaders.o

.PHONY: all clean

all: $(TARGETS)

clean:
	rm $(TARGETS) $(OBJ)

$(OBJ): %.o : src/%.cpp
	$(CC) -c -o $@ $< $(CCFLAGS)

$(TARGETS): % : $(OBJ) %.o
	$(CC) -o $@ $^ $(CCFLAGS) $(LDFLAGS)
