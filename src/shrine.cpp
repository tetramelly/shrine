/*
 * shrine.cpp: main game loop
 *
 * Copyright (C) 2020 Dani

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstdio>

#include "input.h"
#include "shaders.h"
#include "vertices.h"

#define SCREEN_WIDTH    1920
#define SCREEN_HEIGHT   1080

int gl_game_setup()
{
    GLuint vbo, vso, fso, vao, ebo, tex;
    GLint status;

    // vaos store all links between attribs and vbos with raw vertex data
    // use when using different shaders and vertex layouts, and changing the active
    //  shader program
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // repeat texture and don't filter
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    int width, height;
    unsigned char * image =
        SOIL_load_image("img/borzoi-clothed.jpg", &width, &height, 0, SOIL_LOAD_RGB);
    float aspect_ratio = (float)width / (float)height;
    for (int i = 0; i < 4; i++) vertices[i * 5] *= aspect_ratio;
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
    // params: texture target, LOD, pix format, (width, height), must be 0 (border),
    //  format of pixels, pixels themselves
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
            GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);

    // elements - unsigned ints referring to vertices bound to GL_ARRAY_BUFFER
    GLuint elements[] = {
        0, 1, 2,
        2, 3, 0
    };
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

    // allows uploading vertex data to graphics card
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo); // make vbo the active buffer object
    // static draw: vertex data uploaded once and drawn many times
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // create shader object and load code onto it
    vso = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vso, 1, &vertex_shader, NULL);
    glCompileShader(vso);

    // check if shader compiled successfully
    glGetShaderiv(vso, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
        puts("shader compilation failed for vertex shader");
        return -1;
    }

    fso = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fso, 1, &fragment_shader, NULL);
    glCompileShader(fso);

    glGetShaderiv(fso, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
        puts("shader compilation failed for fragment shader");
        return -1;
    }

    // program to combine two shaders together
    GLuint shader_program = glCreateProgram();
    glAttachShader(shader_program, vso);
    glAttachShader(shader_program, fso);
    glBindFragDataLocation(shader_program, 0, "outColor");
    glLinkProgram(shader_program);
    glUseProgram(shader_program);

    // reference to position input in vertex shader
    GLint pos_attr = glGetAttribLocation(shader_program, "position");
    // 2nd param: number of values for input, or # of components of vec
    // 5th param: stride - how many bytes between each position attribute in array
    // 6th param: offset - how many bytes from start of array the attribute occurs
    glEnableVertexAttribArray(pos_attr);
    glVertexAttribPointer(pos_attr, 3, GL_FLOAT, GL_FALSE,
            5*sizeof(GLfloat), 0); // 4th param: normalize

    GLint tex_attr = glGetAttribLocation(shader_program, "texcoord");
    glEnableVertexAttribArray(tex_attr);
    glVertexAttribPointer(tex_attr, 2, GL_FLOAT, GL_FALSE,
            5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));

    glm::mat4 model = glm::mat4(1.0f);
    model = glm::rotate(model, glm::radians(20.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    GLint uni_model = glGetUniformLocation(shader_program, "model");
    glUniformMatrix4fv(uni_model, 1, GL_FALSE, glm::value_ptr(model));

    // view transformation: simulates moving camera
    glm::mat4 view = glm::lookAt(
            glm::vec3(1.2f, 1.2f, 1.2f),
            glm::vec3(0.0f, 0.0f, 0.0f),
            glm::vec3(0.0f, 0.0f, 1.0f));
    GLint uni_view = glGetUniformLocation(shader_program, "view");
    glUniformMatrix4fv(uni_view, 1, GL_FALSE, glm::value_ptr(view));

    // projection matrix: provides perspective projection
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 1.0f, 10.0f);
    GLint uni_proj = glGetUniformLocation(shader_program, "proj");
    glUniformMatrix4fv(uni_proj, 1, GL_FALSE, glm::value_ptr(proj));


    glfwSwapInterval(1);
    return 0;
}

/* Window Shit:
 * void window_shit(void);
 * Does Window Shit
 */
void window_shit()
{
    // require opengl context to support version >=3.2
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    // only support new core functionality
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}

int main()
{
    GLFWwindow * window;

    if (!glfwInit()) {
        puts("The program failed to start because GLFW couldn't initialize");
        return -1;
    }
    // window shit
    window_shit();
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Shrine", NULL, NULL);
    if (!window) {
        glfwTerminate();
        puts("The program failed to create a window");
        return -1;
    }
    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    glewInit();

    if (gl_game_setup()) return -1;

    glfwSetKeyCallback(window, key_callback);
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT);

        // uses element array to determine draw order
        // 2nd param: # of indices to draw
        // 3rd param: type of element data
        // 4th param: offset
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
