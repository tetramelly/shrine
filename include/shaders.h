#ifndef SHADERS_H
#define SHADERS_H

#include <GL/glew.h>

extern const GLchar * vertex_shader;
extern const GLchar * fragment_shader;

#endif // SHADERS_H
